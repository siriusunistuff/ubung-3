# -*- coding: utf-8 -*-
"""
Created on Sat Jun 10 12:43:50 2017

@author: Thomas
"""
import re
import os
import codecs
import random
import math
from collections import Counter
from stemming.porter2 import stem

# Get stopwords
directory = os.path.dirname(os.path.realpath(__file__))
path_stopwords = os.path.join(directory, 'english')
file_stopwords = codecs.open(path_stopwords, 'r', "utf-8")
stopwords = file_stopwords.read().splitlines()
file_stopwords.close()


def load_file(filename):
    # 1 - Document zu Wort-Liste (mit Anzahl)
    with open(filename) as f:
        content = re.sub('<.*?>', '', f.read().lower()) # removes html tags
        content = re.findall(r"[a-z0-9\-]*[a-z][a-z0-9\-]*", content) # get all words
        words = Counter(content)
    
    # 2.1 - stopword filter
    for w in stopwords:
        if w in words:
            del words[w]
    
    # 2.2 - Stem­ming (The Porter Stemming Algorithm)
    new_words = Counter()
    for w in words:
        new_w = stem(w)
        new_words[new_w] = new_words[new_w] + words[w]
    words = new_words
    
    return words

# All files
course_files = os.listdir(os.path.join(directory, 'course'))
non_course_files = os.listdir(os.path.join(directory, 'non-course'))

# Subset of all files for training (half of both classes)
course_files_train = random.sample(course_files, len(course_files) // 2)
course_files_test = list(set(course_files) - set(course_files_train))

non_course_files_train = random.sample(non_course_files, len(non_course_files) // 2)
non_course_files_test = list(set(non_course_files) - set(non_course_files_train))

files_train = \
    [os.path.join(directory, 'course', f) for f in course_files_train] + \
    [os.path.join(directory, 'non-course', f) for f in non_course_files_train]
files_test = \
    [os.path.join(directory, 'course', f) for f in course_files_test] + \
    [os.path.join(directory, 'non-course', f) for f in non_course_files_test]

# Load all trainings files
print('Loading files ... ',  end='')
docs_train = [load_file(f) for f in files_train]
docs_test = [load_file(f) for f in files_test]

terms = list(set([item for sublist in docs_train for item in sublist.keys()]))
print('Done!')

# Compute TF-IDF (SMART variant from slides)
print('Computing TF-IDF ... ',  end='')
d_i_j_train = [[0 for t in terms] for d in docs_train]
d_i_j_test = [[0 for t in terms] for d in docs_test]
            
len_D = len(docs_train)
len_D_t_j = [0 for t in terms]
for j in range(len(terms)):
    for d in docs_train:
        if terms[j] in d:
            len_D_t_j[j] += 1

for j in range(len(terms)):
    IDF = math.log((1 + len_D) / len_D_t_j[j])
    
    for i in range(len(docs_train)):
        if docs_train[i][terms[j]] == 0:
            TF = 0
        else:
            TF = 1 + math.log(1 + math.log(docs_train[i][terms[j]]))
        d_i_j_train[i][j] = TF * IDF
    
    for i in range(len(docs_test)):
        if docs_test[i][terms[j]] == 0:
            TF = 0
        else:
            TF = 1 + math.log(1 + math.log(docs_test[i][terms[j]]))
        d_i_j_test[i][j] = TF * IDF
print('Done!')

# Compute the N most relevant features
print('# of terms: ' + str(len(terms)))
print('Computing N most relevant features ... ',  end='')
N = 1000
N_top = Counter()
for j in range(len(terms)):
    N_top[j] = len_D_t_j[j]/len_D
N_top = sorted(N_top, key = N_top.get, reverse = True)[:N]
print('Done!')

# Compute sparse representation
print('Computing sparse representation ... ',  end='')
with open('file_train', 'w') as file_train:
    for i in range(len(docs_train)):
        if re.search(r'\\course\\', files_train[i]):
            c = 1
        else:
            c = -1
        l = str(c)
        for a in range(len(N_top)):
            if d_i_j_train[i][N_top[a]] != 0:
                l += ' ' + str(a) + ':' + str(d_i_j_train[i][N_top[a]])
        #for (a,b) in d_i_j_train_sparse[i]:
        #    if a in N_top:
        #        l += ' ' + str(N_top.index(a)) + ':' + str(b)
        file_train.write( l + '\n')
        
with open('file_test', 'w') as file_test:
    for i in range(len(docs_test)):
        if re.search(r'\\course\\', files_test[i]):
            c = 1
        else:
            c = -1
        l = str(c)
        for a in range(len(N_top)):
            if d_i_j_test[i][N_top[a]] != 0:
                l += ' ' + str(a) + ':' + str(d_i_j_test[i][N_top[a]])
        file_test.write( l + '\n')

print('Done!')
print()



def load_file_prints(filename):
    # 1 - Document zu Wort-Liste (mit Anzahl)
    with open(filename) as f:
        content = re.sub('<.*?>', '', f.read().lower()) # removes html tags
        content = re.findall(r"[a-z0-9\-]*[a-z][a-z0-9\-]*", content) # get all words
        words = Counter(content)
    print(words)
    print(len(words))
    print('-------------------------------------------')
    
    # 2.1 - stopword filter
    for w in stopwords:
        if w in words:
            del words[w]
    print(words)
    print(len(words))
    print('-------------------------------------------')
    
    # 2.2 - Stem­ming (The Porter Stemming Algorithm)
    new_words = Counter()
    for w in words:
        new_w = stem(w)
        new_words[new_w] = new_words[new_w] + words[w]
    words = new_words
    print(words)
    print(len(words))
    
    return words

doc_print = load_file_prints('course/http_^^cs.cornell.edu^Info^Courses^Current^CS415^CS414.html')

d_i_j_print = [0 for t in terms]
for j in range(len(terms)):
    IDF = math.log((1 + len_D) / len_D_t_j[j])
    
    if doc_print[terms[j]] == 0:
        TF = 0
    else:
        TF = 1 + math.log(1 + math.log(doc_print[terms[j]]))
    d_i_j_print[j] = TF * IDF

c = -1
l = str(c)
for a in range(len(N_top)):
    if d_i_j_print[N_top[a]] != 0:
        l += ' ' + str(a) + ':' + str(d_i_j_print[N_top[a]])

#print(l)
#print(N_top)
#print([terms[a] for a in N_top])
















