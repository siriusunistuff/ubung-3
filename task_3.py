# -*- coding: utf-8 -*-
"""
Created on Sat Jun 10 12:43:50 2017

@author: Thomas
"""

import re
import matplotlib.pyplot as plt

probs = []
with open('file_test.out') as f:
    content = f.read().split("\n")
    for line in content:
        if re.match('^labels', line) or line == '':
            continue
        _, p, _ = line.split();
        probs.append(float(p))

classes = []
with open('file_test') as f:
    content = f.read().split("\n")
    for line in content:
        if line == '':
            continue
        c = line.split()[0];
        classes.append(float(c))

if len(probs) != len(classes):
    raise Exception('BLOB')

def recall_precision(limit, s):
    tp = 0; fp = 0; tn = 0; fn = 0;
    for i in range(len(probs)):
        p = probs[i]
        c = classes[i]
        
        if p >= limit:
            if c == 1:
                tp += 1
            else:
                fp += 1
        else:
            if c == 1:
                fn += 1
            else:
                tn += 1
    if tp == 0:
        return -1, -1, s
    else:
        rec = tp/(tp+fn)
        pre = tp/(tp+fp)
        if s == 0 and pre >= rec:
            s = 1
            print('---------------------------')
        print(limit, pre, rec)
        print('   ' + str((tp+tn)/(tp+tn+fp+fn)))
        return rec, pre, s

n = 100
limits = [i/n for i in range(n+1)]

recall = []
precision = []
s = 0
for l in limits:
    r, p, s = recall_precision(l, s)
    if r == -1:
        continue;
    recall.append(r)
    precision.append(p)


plt.plot(recall, precision)
plt.plot([0,1], [0,1])
plt.title('Recall-Precision')
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.show()



N = [5, 10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120, 10227]
T = [0.035, 0.038, 0.077, 0.249, 0.088, 0.056, 0.060, 0.084, 0.135, 0.127, 0.146, 0.242]
A = [82.67, 85.36, 87.07, 92.40, 93.45, 95.62, 96.87, 96.77, 94.49, 94.30, 94.68, 94.68]

plt.plot(N, A)
plt.title('3.4')
plt.xlabel('N')
plt.xscale('log')
plt.ylabel('Accuracy')
plt.show()


plt.plot(N, T)
plt.title('3.4')
plt.xlabel('N')
plt.xscale('log')
plt.ylabel('Time')
plt.show()














